const Helper = {};

Helper.entierAleatoire = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

Helper.guidGenerator = () => {
    const S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

Helper.max = (int1, int2) => {
    return (int1 > int2) ? int1 : int2;
}

Helper.min = (int1, int2) => {
    return (int1 < int2) ? int1 : int2;
}

Helper.getSum = (total, num) => {
    return total + num;
}

module.exports = Helper;
