const express = require('express');
const app = express();
const http = require('http').Server(app);
const cors = require('cors');
const bodyParser = require('body-parser');

const api = require('./api/routes.js');
const sequelize = require('./database/connection');
const user = require('./database/models/user');
const personne = require('./database/models/personne');
const lien = require('./database/models/lien');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.options('*', cors());
app.use('/api', api);
app.use(express.static('assets'));

sequelize.authenticate()
	.then(async () => {
		console.log('Connection has been established successfully.');
		const users = await user.findAll();
		const peronnes = await personne.findAll();		
		const liens = await lien.findAll();
	})
	.catch(err => {
		console.error('Unable to connect to the database:', err);
	});

const port = 8100;
const ip = '0.0.0.0';
http.listen(port, ip, function () {
	console.log('listening on *:' + port);
});
