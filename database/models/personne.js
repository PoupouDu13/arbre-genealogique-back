const Sequelize = require('sequelize');
const sequelize = require('../connection');
const User = require('./user');

const Personne = sequelize.define('personne', {
    nom: {
        type: Sequelize.STRING
    },
    prenom: {
        type: Sequelize.STRING
    },
    generationId: {
        type: Sequelize.INTEGER
    },
    dateNaissance: {
        type: Sequelize.DATEONLY
    },
    dateDeces: {
        type: Sequelize.DATEONLY
    },
    genre: {
        type: Sequelize.ENUM('homme', 'femme')
    }
});

Personne.belongsTo(User);
sequelize.sync();

module.exports = Personne;