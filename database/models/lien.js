const Sequelize = require('sequelize');
const sequelize = require('../connection');
const User = require('./user');

const Lien = sequelize.define('lien', {
    type: {
        type: Sequelize.STRING
    },
    source: {
        type: Sequelize.INTEGER
    },
    target: {
        type: Sequelize.INTEGER
    },
});

Lien.belongsTo(User);
sequelize.sync();

module.exports = Lien;