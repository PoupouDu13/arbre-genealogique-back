const Sequelize = require('sequelize');
const sequelize = require('../connection');

const User = sequelize.define('user', {
    username: {
        type: Sequelize.STRING,
        unique: true 
    },
    email: {
        type: Sequelize.STRING,
        unique: true 
    },
    password: {
        type: Sequelize.STRING
    },
    guid_token: {
        type: Sequelize.STRING
    },
    active: {
        type: Sequelize.BOOLEAN, defaultValue: false
    },
    session_guid: {
        type: Sequelize.STRING
    },
});

sequelize.sync();

module.exports = User;