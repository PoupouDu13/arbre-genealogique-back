const Sequelize = require('sequelize');
/*const sequelize = new Sequelize(
    process.env.MYSQL_DATABASE || 'arbregenealogique',
    process.env.MYSQL_USER || 'root',
    process.env.MYSQL_PASSWORD || 'password',
    {
        host: process.env.MYSQL_SERVER || 'localhost',
        dialect: 'mysql',
        operatorsAliases: false,
    });*/
const sequelize = new Sequelize(
    'arbregenealogique_db',
    '185283',
    'mysqlpassword',
    {
        host: 'mysql-arbregenealogique.alwaysdata.net',
        dialect: 'mysql',
        operatorsAliases: false,
    });

module.exports = sequelize;