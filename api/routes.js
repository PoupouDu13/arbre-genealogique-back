const express = require('express');
const jwt = require('express-jwt');
const router = express.Router();
const user = require('./user.js');
const personne = require('./personne');
const upload = require('./upload');

const JWT_SECRET = require('../helpers/constants').JWT_SECRET;

router.use('/user', user);
router.use('/personne', jwt({ secret: JWT_SECRET }), personne);
router.use('/upload', upload);

module.exports = router;