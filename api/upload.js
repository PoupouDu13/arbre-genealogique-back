const express = require('express');
const router = express.Router();
const formidable = require('formidable');
const decode = require('jwt-decode');
const mv = require('mv');
const fs = require('fs');
const simpleLogger = require('simple-node-logger').createSimpleLogger('project.log');

router.post('/uploadPhoto', async function (req, res) {
    try {
        simpleLogger.info('upload yala');
        const form = new formidable.IncomingForm();
        simpleLogger.info('form : ', form);
        form.parse(req, function (err, fields, files) {
            simpleLogger.info('err : ', err);
            simpleLogger.info('fields : ', fields);
            simpleLogger.info('files : ', files);
            const userId = decode(fields.token).id;
            const newpathFolder = `/home/arbregenealogique/www/arbre-genealogique-back/assets/portraits/${userId}`;/*`assets/portraits/${userId}`;*/
            if (!fs.existsSync(newpathFolder)) {
                fs.mkdirSync(newpathFolder);
            }
            const oldpath = files.image.path.replace(/\\/, "/");
            const fileType = files.image.type
            if (fileType.startsWith('image/')) {
                simpleLogger.info('oldpath :', oldpath);
                const newpath = `${newpathFolder}/${fields.personneId}.png`;
                simpleLogger.info('newpath : ', newpath);
                mv(oldpath, newpath, function (err) {
                    simpleLogger.info('err : ', err);
                    res.json({ newpath });
                })
            } else {
                res.status(400).json({
                    message: 'wrong image format'
                })
            }

        });
    } catch (error) {
        simpleLogger.info('error : ', error);
        res.json(error);
    }
});

module.exports = router;