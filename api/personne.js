const express = require('express');
const router = express.Router();
const decode = require('jwt-decode');
const Personne = require('../database/models/personne');
const Lien = require('../database/models/lien');

router.post('/addFirstPersonne', async function (req, res) {
    try {
        const userId = decode(req.body.token).id;
        const result = await Personne.create({
            nom: req.body.nom,
            prenom: req.body.prenom,
            generationId: req.body.generationId,
            dateNaissance: req.body.dateNaissance,
            dateDeces: req.body.dateDeces,
            genre: req.body.genre,
            userId,
        });
        res.json(result);
    } catch (error) {
        console.log(error);
    }
});

router.post('/addDescendant', async function (req, res) {
    try {
        const userId = decode(req.body.token).id;
        const personne = await Personne.create({
            nom: req.body.nom,
            prenom: req.body.prenom,
            dateNaissance: req.body.dateNaissance,
            dateDeces: req.body.dateDeces,
            genre: req.body.genre,
            generationId: req.body.generationId,
            userId,
        });
        const lien1 = await Lien.create({
            type: req.body.type,
            source: personne.id,
            target: req.body.personne1Id,
            userId,
        });
        const lien2 = await Lien.create({
            type: req.body.type,
            source: personne.id,
            target: req.body.personne2Id,
            userId,
        });
        res.json(personne);
    } catch (error) {
        console.log(error);
    }
});

router.post('/addPartenaire', async function (req, res) {
    try {
        const userId = decode(req.body.token).id;
        const personne = await Personne.create({
            nom: req.body.nom,
            prenom: req.body.prenom,
            dateNaissance: req.body.dateNaissance,
            dateDeces: req.body.dateDeces,
            genre: req.body.genre,
            generationId: req.body.generationId,
            userId,
        });
        const result = await Lien.create({
            type: req.body.type,
            source: req.body.personneId,
            target: personne.id,
            userId,
        });
        res.json(personne);
    } catch (error) {
        console.log(error);
    }
});

router.post('/modifiePersonne', async function (req, res) {
    try {
        const userId = decode(req.body.token).id;
        const personne = await Personne.findById(req.body.id);
        personne.update({
            nom: req.body.nom,
            prenom: req.body.prenom,
            dateNaissance: req.body.dateNaissance,
            dateDeces: req.body.dateDeces,
            genre: req.body.genre,
            userId
        }).then((result) => {
            console.log(result);

            res.json(result);
        })
    } catch (error) {
        console.log(error);
    }
});

router.post('/deletePersonneMarier', async function (req, res) {
    try {
        const userId = decode(req.body.token).id;
        const deletedPersonne = await Personne.destroy({
            where: {
                id: req.body.id,
                userId,
            }
        });
        const deletedLink = await Lien.destroy({
            where: {
                type: 'MARIER',
                target: req.body.id,
                userId,
            }
        })
        res.json({ deletedPersonne, deletedLink });
    } catch (error) {
        console.log(error);
    }
});

router.post('/deletePersonneCelibataire', async function (req, res) {
    try {
        const userId = decode(req.body.token).id;
        const deletedPersonne = await Personne.destroy({
            where: {
                id: req.body.id,
                userId,
            }
        });
        const deletedLink = await Lien.destroy({
            where: {
                type: 'ENFANT_DE',
                source: req.body.id,
                userId,
            }
        })
        res.json({ deletedPersonne, deletedLink });
    } catch (error) {
        console.log(error);
    }
});

router.post('/getTree', async function (req, res) {
    const userId = decode(req.body.token).id;
    const personnesResult = await Personne.findAll({
        where: {
            userId
        }
    });
    const linksResult = await Lien.findAll({
        where: {
            userId
        }
    });
    const maxDepth = await Personne.max('generationId', {
        where: {
            userId
        }
    });
    const links = linksResult.map(linkResult => linkResult.dataValues);
    const nodes = personnesResult.map(personneResult => personneResult.dataValues);

    res.json({ links, nodes, maxDepth })
});

module.exports = router;