const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const md5 = require('md5');
const User = require('../database/models/user.js');
const Helper = require('../helpers/helper');
const JWT_SECRET = require('../helpers/constants').JWT_SECRET;

router.post('/insertUser', function (req, res) {
    User.create({
        username: req.body.username,
        email: req.body.email,
        password: md5(req.body.password),
    }).then(function (result) {
        const { id, username, email } = result;        
        const token = jwt.sign({
            id,
            username,
            email
        }, JWT_SECRET);        
        
        res.json({
            id,
            username,
            email,
            token
        })
    }).catch(function (error) {
        res.status(500).send("Pseudo ou email non valide/existant.")
    });
});

router.post('/login', function (req, res) {
    User.findOne({
        where: {
            username: req.body.username,
            password: md5(req.body.password),
        }
    }).then(function (result) {
        if (!result) {
            return res.status(400).json({
                message: 'Bad credentials'
            })
        }

        const { id, username, email } = result;
        const token = jwt.sign({
            id,
            username,
            email
        }, JWT_SECRET);

        res.json({
            id,
            username,
            email,
            token
        })
    }).catch(function (error) {
        res.status(500).send("Pseudo ou email non valide/existant.")
    });
});

module.exports = router;